<?php
class JumbotronShortcode {
	public function __construct() {
		$this->tag = "jumbotron";
	}

	public function handleShortcode($attributes) {
		$a = shortcode_atts( array(
			'title' => "default title",
			'subtitle' => "default subtitle"
		), $attributes );
		ob_start(); ?>

		<div class="jumbotron">
			<h1 class="title container"><?php echo $a['title'] ?></h1>
			<h2 class="subtitle container"><?php echo $a['subtitle'] ?></h2>
		</div>

<?php 	return ob_get_clean();;
	}
}


$jumbotronShortcode = new JumbotronShortcode();
add_shortcode($jumbotronShortcode->tag, [$jumbotronShortcode, 'handleShortcode']);
