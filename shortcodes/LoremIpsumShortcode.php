<?php
class LoremIpsumShortcode {
	public function __construct() {
		$this->tag = "lorem-ipsum";
	}

	public function handleShortcode($attributes) {
		$a = shortcode_atts( array(
			'paragraphs' => "1",
			'sentences' => "1",
			'words' => '2'
		), $attributes );

		return $this->generateLoremIpsum($a['paragraphs'], $a['sentences'], $a['words']);
	}

	public function generateLoremIpsum($numParagraphs = 1, $numSentences = 1, $numWords = 2) {

		$words = ["lorem", "ipsum", "dolor", "sit", "amet", "consectetuer",
		"adipiscing", "elit", "sed", "diam", "nonummy", "nibh", "euismod",
		"tincidunt", "ut", "laoreet", "dolore", "magna", "aliquam", "erat"];

		$result = "";
		for($p = 0; $p < $numParagraphs; $p++) {
			$result .= "<p>";
			for($s = 0; $s < $numSentences; $s++) {
				for($w = 0; $w < $numWords; $w++) {
					if ($w > 0) { $result.= " "; }
					$result .= $words[rand(0, count($words)-1)];
				}
				$result .= ". ";
			}
			$result .= "</p>";
		}

		return $result;
	}
}

$loremIpsumShortcode = new LoremIpsumShortcode();
add_shortcode($loremIpsumShortcode->tag, [$loremIpsumShortcode, 'handleShortcode']);
