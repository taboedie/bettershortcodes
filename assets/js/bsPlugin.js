import BsEventManager from 'BsEventManager.js'
import * as Shortcodes from 'shortcodes.js'
console.log(Shortcodes)

export default class BsPlugin {
  constructor (editor, url) {
    this.editor = editor
    this.url = url
    this.pluginName = 'BetterShortcodes'
    this.eventManager = new BsEventManager(editor)
    this.shortcodes = []
    this.initPlugin()
  }

  initPlugin () {
    this.editor.addButton(this.pluginName, {
      icon: 'bs_shortcode',
      tooltip: this.pluginName,
      onclick: event => this.openWindow(event)
    })

    BsEventManager.DispatchEvent({
      name: 'betterShortcodesReady',
      payload: {
        editor: this.editor,
        url: this.url,
        register: (args) => this.registerShortcode(args)
      }
    })
  }

  openWindow (event) {
    this.window = this.editor.windowManager.open({
      title: this.pluginName,
      body: this.shortcodes.map(item => item.getListItem()),
      width: 200,
      height: 400
    })
  }

  registerShortcode (shortcode) {
    this.shortcodes.push(shortcode)
  }
}
