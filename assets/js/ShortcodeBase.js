import BsUtil from 'BsUtil.js'
const { ajaxurl, jQuery } = window

export default class ShortcodeBase {
  constructor ({editor, url, tag, shortcodeName}) {
    this.editor = editor
    this.url = url
    this.shortcodeName = shortcodeName
    this.tag = tag
    this.cache = []
    this.defaultMceArgs = {
      no_events: false,
      force_br_newlines: false,
      force_p_newlines: false,
      forced_root_block: ''
    }
    document.addEventListener('replaceShortcodes', e => this.matchShortcodes(e))
    document.addEventListener('restoreShortcodes', e => this.restoreShortcode(e))
    // this.editor.on('mouseup', (e) => this.onMouseUp(e))
  }

  getListItem () {
    if (this.listItem) { return this.listItem }

    return window.tinymce.ui.Factory.create({
      type: 'button',
      text: this.shortcodeName
    }).on('click', e => {
      this.openWindow(e)
    })
  }

  onMouseUp (event) {
    console.log(this.cache)
    console.log(event)
  }

  insertContent (content) {
    this.editor.insertContent(content)
  }

  matchShortcodes (event) {
    var matches = BsUtil.Match(this.tag, this.editor.getContent(this.defaultMceArgs))
    if (matches == null) { return }
    matches.map(match => this.getShortcodeHtml(match, this.replaceShortcode.bind(this)))
  }

  getShortcodeHtml (shortcode, callback) {
    jQuery.post(ajaxurl,
      {
        action: 'render_shortcode',
        shortcode,
        tag: this.tag
      }, callback, 'json')
  }

  replaceShortcode (response) {
    const shortcode = response.data.shortcode
    const html = BsUtil.FilterHtml(response.data.html)
    var content = this.editor.getContent(this.defaultMceArgs)
    // content = BsUtil.StripTagsAroundShortcode(content, shortcode)
    this.editor.setContent(content.replace(shortcode, html), this.defaultMceArgs)
    this.cache.push({shortcode, html})
  }

  restoreShortcode (event) {
    this.cache.map(item => {
      var {html, shortcode} = item
      var content = this.editor.getContent(this.defaultMceArgs)
      content = content.replace(html, shortcode)
      this.editor.setContent(content, {format: 'raw', no_events: true})
    })

    document.querySelector('.wp-editor-area').value = this.editor.getContent(this.defaultMceArgs)
    this.cache = []
  }
}
