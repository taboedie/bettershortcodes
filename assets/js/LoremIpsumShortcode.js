import ShortcodeBase from 'ShortcodeBase.js'

export default class LoremIpsumShortcode extends ShortcodeBase {
  constructor ({editor, url}) {
    var shortcodeName = 'Lorem Ipsum'
    var tag = 'lorem-ipsum'
    super({editor, url, tag, shortcodeName})
  }

  openWindow (event) {
    window.tinymce.activeEditor.windowManager.close()
    this.editor.windowManager.open({
      title: 'Lorem Ipsum generator',
      body: [
        {
          type: 'textbox',
          name: 'paragraphs',
          label: 'Paragraphs',
          tooltip: 'Specify the amount of paragraphs',
          value: '1'
        },
        {
          type: 'textbox',
          name: 'sentences',
          label: 'Sentences',
          tooltip: 'Specify the amount of sentences',
          value: '1'
        },
        {
          type: 'textbox',
          name: 'words',
          label: 'Words',
          tooltip: 'Specify the wordcount',
          value: '2'
        }
      ],
      onsubmit: e => this.onSubmit(e)
    })
  }

  onSubmit (event) {
    const {paragraphs, sentences, words} = event.data
    var shortcode = `[lorem-ipsum paragraphs="${paragraphs}" sentences="${sentences}" words="${words}"]`
    this.insertContent(shortcode)
  }
}

document.addEventListener('betterShortcodesReady', (event) => {
  const { editor, url, register } = (event.detail)
  register(new LoremIpsumShortcode({editor, url}))
})
