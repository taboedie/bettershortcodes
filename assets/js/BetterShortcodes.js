import BsPlugin from 'BsPlugin.js'

window.jQuery(document).ready(event => {
  window.tinymce.PluginManager.add('BetterShortcodes', BsPlugin)
  console.log('BetterShortcodes initialized')
})
