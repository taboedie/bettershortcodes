const { jQuery } = window
const $ = jQuery

export default class BsEventManager {
  constructor (editor) {
    this.editor = editor
    this.document = $(document)
    this.textEditor = $('#content')
    this.editor.on('show', (e) => this.onMceShow(e))
    this.editor.on('change', (e) => this.onMceShow(e))
    this.editor.on('hide', (e) => this.onMceHide(e))
    this.document.on('tinymce-editor-init.editor-expand', e => this.onTinyMCEInit(e))
  }

  onTinyMCEInit (event, editor) {
    this.fireInitialEvent()
  }

  getCurrentMode () {
    return $('#wp-content-wrap').hasClass('tmce-active') ? 'tinymce' : 'html'
  }

  fireInitialEvent () {
    var isVisual = $('#wp-content-wrap').hasClass('tmce-active')
    if (isVisual) {
      this.onMceShow(this.editor)
    } else {
      this.onMceHide(this.editor)
    }
  }

  onMceShow (event) {
    console.log('onMceShow')
    BsEventManager.DispatchEvent({name: 'replaceShortcodes'})
  }
  onMceHide (event) {
    console.log('onMceHide')
    BsEventManager.DispatchEvent({name: 'restoreShortcodes'})
  }

  onTinceMCEInitialize (event, editor) {
    console.log('onTinceMCEInitialize')
  }
  /**
  * Create custom 'betterShortcodesReady' event
  * @return {CustomEvent} betterShortcodesReady event
  */
  static CreateBsEvent ({name, payload, dispatch = false}) {
    var event = document.createEvent('customEvent')
    event.initCustomEvent(name, true, true, payload)
    return event
  }

  static DispatchEvent ({name, payload = {}, dispatch = true, event}) {
    if (!event) { event = BsEventManager.CreateBsEvent({name, payload, dispatch}) }
    document.dispatchEvent(event)
  }
}
