export default class BsUtil {
  /**
  * Filters html with tinymce
  * @param {string}} html
  * @return {string} filtered html
  */
  static FilterHtml (html) {
    var parser = new window.tinymce.html.DomParser({validate: true}, window.tinymce.activeEditor.schema)
    var escapedHtml = html.replace(/\\"/g, '"')
    var parsedData = parser.parse(escapedHtml)
    return new window.tinymce.html.Serializer().serialize(parsedData)
  }

  static Match (tag, text) {
    return text.match(window.wp.shortcode.regexp(tag))
  }
  // https://stackoverflow.com/a/32576376
  static StripTagsAroundShortcode (content, shortcode) {
    var tags = content.match(/<\s*p[^>]*>([^<]*)<\s*\/\s*p\s*>/g)
    var filteredTags = tags.filter(tag => tag.includes(shortcode))
    filteredTags.forEach((tag) => {
      var strippedTag = tag.replace(/<.*?>/g, '')
      content = content.replace(tag, strippedTag)
    })

    return content.trim()
  }
}

window.BsUtil = BsUtil
