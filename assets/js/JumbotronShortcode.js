import ShortcodeBase from 'ShortcodeBase.js'

export default class JumbotronShortcode extends ShortcodeBase {
  constructor ({editor, url}) {
    var shortcodeName = 'Jumbotron'
    var tag = 'jumbotron'
    super({editor, url, tag, shortcodeName})
  }

  openWindow (event) {
    window.tinymce.activeEditor.windowManager.close()
    this.editor.windowManager.open({
      title: 'Jumbotron generator',
      body: [
        {
          type: 'textbox',
          name: 'title',
          label: 'Title',
          value: ''
        },
        {
          type: 'textbox',
          name: 'subtitle',
          label: 'Subtitle',
          value: ''
        }
      ],
      onsubmit: e => this.onSubmit(e)
    })
  }

  onSubmit (event) {
    const { title, subtitle } = event.data
    var shortcode = `[${this.tag} title="${title}" subtitle="${subtitle}"]`
    this.insertContent(shortcode)
  }
}

document.addEventListener('betterShortcodesReady', (event) => {
  const { editor, url, register } = (event.detail)
  register(new JumbotronShortcode({editor, url}))
})
