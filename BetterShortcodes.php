<?php
/*
Plugin Name: BetterShortcodes
Plugin URI:
Description: A plugin to add better Shortcodes
Version: 1.0
Author: Ruud Schop
Author URI: https://RuudSchop.nl
*/

require_once("shortcodes/LoremIpsumShortcode.php");
require_once("shortcodes/JumbotronShortcode.php");
/*******
* TODO
* -List all registered BetterShortcodes codes
* -Specific option screen per shortcode
*******/

class BetterShortcodes {
	private $pluginName = "BetterShortcodes";
	private static $instance;

	public static function get_instance() {
		if(self::$instance === null)
		self::$instance = new self();

		return self::$instance;
	}

	private function __construct($args = array()){

		if ( is_admin() ){
			add_action('wp_ajax_nopriv_render_shortcode',[$this, 'render_shortcode']);
			add_action('wp_ajax_render_shortcode', [$this, 'render_shortcode']);
			add_action('admin_head', array( $this, 'admin_head') );
			add_action('admin_enqueue_scripts', array($this , 'enqueue_scripts' ) );
		}else {
			add_action('wp_enqueue_scripts', array($this , 'enqueue_scripts' ) );
		}
	}

	function mce_external_plugins( $plugin_array ) {
		$plugin_array[$this->pluginName] = plugins_url( 'dist/BetterShortcodes.js' , __FILE__ );
		return $plugin_array;
	}

	function mce_buttons( $buttons ) {
		array_push( $buttons, $this->pluginName );
		return $buttons;
	}

	function admin_head() {
		if (!current_user_can( 'edit_posts') && !current_user_can('edit_pages')) {
			return;
		}

		if (get_user_option( 'rich_editing') == 'true') {
			//echo "<script> window.shortcode_list = {$this->serialize()} </script>";
			add_filter( 'mce_css', array( $this , 'plugin_mce_css' ) );
			add_filter( 'mce_external_plugins', array( $this ,'mce_external_plugins' ) );
			add_filter( 'mce_buttons', array($this, 'mce_buttons' ) );
		}
	}

	function plugin_mce_css($mce_css) {
		if ( ! empty( $mce_css ) )
		$mce_css .= ',';

		$mce_css .= plugins_url( 'dist/mce_style.css', __FILE__ );

		return $mce_css;
	}

	function enqueue_scripts() {
		wp_enqueue_style('bs-bettershortcodes-style', plugins_url( 'dist/mce_style.css' , __FILE__ ) );
	}

	function render_shortcode() {
		$shortcode = stripcslashes($_POST['shortcode']);

		if($shortcode == do_shortcode($shortcode)) {
			wp_send_json_error([
				'shortcode' => $shortcode,
				'html' =>   do_shortcode($shortcode)
			]);
		}

		$response = [
			'shortcode' => $shortcode,
			'html' => do_shortcode($shortcode)
		];

		wp_send_json_success($response);
	}
}

BetterShortcodes::get_instance();
