const merge = require('webpack-merge')
const config = require('./webpack.config.js')
var BrowserSyncPlugin = require('browser-sync-webpack-plugin')

module.exports = merge(config, {
  devtool: 'inline-source-map',
  devServer: {
    contentBase: './dist'
  },
  plugins: [
    new BrowserSyncPlugin({
      proxy: config.proxyURL,
      files: [
        '**/*.php'
      ],
      reloadDelay: 0
    })
  ]
})
