const path = require('path')
const CleanWebpackPlugin = require('clean-webpack-plugin')
const ExtractTextPlugin = require('extract-text-webpack-plugin')
const CopyWebpackPlugin = require('copy-webpack-plugin')
const StyleLintPlugin = require('stylelint-webpack-plugin')
// const webpack = require('webpack')

module.exports = {
  entry: {
    BetterShortcodes: './assets/js/BetterShortcodes.js',
    mce_style: './assets/css/mce_style.css',
    style: './assets/css/style.css'
  },
  output: {
    filename: '[name].js',
    path: path.resolve(__dirname, 'dist')
  },
  resolve: {
    extensions: ['.js', '.jsx', '.css'],
    modules: ['node_modules', 'assets/js']
  },
  externals: {
    jquery: 'jQuery'
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /(node_modules|bower_components)/,
        loader: 'babel-loader'
      },
      {
        test: /\.css$/,
        use: ExtractTextPlugin.extract({
          fallback: 'style-loader',
          use: [
            { loader: 'css-loader', options: {importLoaders: 1} },
            'postcss-loader'
          ]
        })
      },
      {
        test: /\.(png|jpg|gif|svg)$/,
        loader: 'url-loader',
        options: {
          limit: 8192
        }
      }
    ]
  },
  plugins: [
    new CleanWebpackPlugin(['dist/*']),
    new ExtractTextPlugin('[name].css'),
    new StyleLintPlugin({files: './assets/css/**/*.css'}),
    new CopyWebpackPlugin([
      {from: 'assets/img', to: 'img'}
    ])
  ]
}
