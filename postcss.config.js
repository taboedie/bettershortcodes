module.exports = (ctx) => ({
	parser: ctx.parser ? 'sugarss' : false,
	map: ctx.env === 'development' ? ctx.map : false,
	sourceMap: true,
	plugins: {
		'postcss-import': {},
		'postcss-cssnext': {},
		'lost': {},
		'postcss-inline-svg': {},
		'postcss-object-fit-images': {},
		'postcss-url': {from: "/", to: "/"},
		'cssnano': ctx.env === 'production' ? {} : false
	}
})
